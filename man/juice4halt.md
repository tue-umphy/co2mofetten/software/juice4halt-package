% juice4halt(1) | manpage

NAME
====

**juice4halt-shutdown** - shut down system before power is lost

DETAILS
=======

This only works with a Juice4Halt Shield attached properly to a Raspberry Pi.
Without the shield, expect useless shutdowns.

SYSTEMD UNIT
============

A systemd template unit file `juice4halt.service` is also shipped.

You need to enable and start it to work:

```bash
# systemctl enable --now juice4halt.service
```

PACKAGER
========

Yann Büchau <nobodyinperson@posteo.de>


SEE ALSO
========

- Upstream: https://github.com/Juice4halt/Shutdown-script
